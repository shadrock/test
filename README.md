# Test

A test repo to see how/if this differs from Ghub. _So far_ it looks about the **same**. Same basic UI, and it supports [Markdown](https://www.markdownguide.org/). The browser editor is nice. Things to look into: 
- templating for issues, etc.
- which desktop text editor works best? I have come to love Atom... 